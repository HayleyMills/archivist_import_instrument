#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to import a xml file
"""

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
import time
import sys
import os


driver = webdriver.Remote(
         command_executor='http://selenium-standalone-firefox:4444/wd/hub',
         desired_capabilities=DesiredCapabilities.FIREFOX)

# driver = webdriver.Firefox(executable_path="/home/jenny/Documents/python_scripts.git/geckodriver")


def archivist_login(url, uname, pw):
    """
    Log in to archivist
    """
    driver.get(url)
    time.sleep(5)
    driver.find_element(By.ID, "email").send_keys(uname)
    driver.find_element(By.ID, "password").send_keys(pw)
    driver.find_element(By.XPATH, '//span[text()="Log In"]').click()
    time.sleep(5)


def upload_instrument(xmlFile):
    """
    Upload DDI Instrument File
    """
    # Upload page
    import_url = "https://closer-build.herokuapp.com/admin/import"
    driver.get(import_url)
    time.sleep(5)

    # Upload DDI Instrument Files
    # Identify element
    s = driver.find_elements(By.XPATH, "//input[@type='file']")[0]
    # File path specified with send_keys
    s.send_keys(os.path.abspath(xmlFile))
    # Import Instrument button
    importButton = driver.find_element(By.XPATH, "//*[contains(text(), 'Import Instrument')]")
    importButton.click()
    try:
        # wait until it is uploaded
        CreatedElement = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH,  "//*[contains(text(), 'Created')]")))
        print(xmlFile + " created!")
    except TimeoutException:
        print("uploading took too much time!")


def upload_check(xmlFiles, log_dir):
    """
    Check the log for the uploaded instrument File
    """
    # Log in
    import_url = "https://closer-build.herokuapp.com/admin/imports/"
    driver.get(import_url)
    time.sleep(5)

    with open(os.path.join(log_dir, 'summary.csv'), "a") as f:
        f.write( ",".join(["File", "State", "Date"]) + "\n")

    # Check if uploaded correctly
    for i in range(len(xmlFiles)):
        # print(i)
        tr = driver.find_elements(By.XPATH, "html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")[i]
        filename = tr.find_elements(By.XPATH, "td")[1].text

        with open(os.path.join(log_dir, 'summary.csv'), "a") as f:
            f.write( ",".join([filename, tr.find_elements(By.XPATH, "td")[3].text, tr.find_elements(By.XPATH, "td")[4].text]) + "\n")

        tr.find_elements(By.XPATH, "td")[-1].click()
        time.sleep(5)

        with open(os.path.join(log_dir, filename + '_log.html'), 'w') as f:
            f.write(driver.page_source)

        driver.back()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    # log in
    archivist_url = "https://closer-build.herokuapp.com"
    archivist_login(archivist_url, uname, pw)

    main_dir = 'input'
    input_names = [f for f in os.listdir(main_dir) if f.endswith('.xml')]
    number_input = len(input_names)

    log_dir = 'log'
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    xmlFiles = [os.path.join(main_dir, input_name) for input_name in input_names]
    # print(xmlFiles)
    for xmlFile in xmlFiles:
        upload_instrument(xmlFile)

    time.sleep(10)
    upload_check(xmlFiles, log_dir)

    driver.quit()


if __name__ == "__main__":
    main()

